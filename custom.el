(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-ghc-show-info t)
 '(package-selected-packages
   (quote
    (ack jammer indent-tools yaml-mode cmake-mode cedit company-c-headers nyan-mode irony rtags cmake-ide auth-password-store js2-mode deft shakespeare-mode company-ghc w3m dictionary yasnippet company latex-preview-pane color-theme-approximate color-theme-solarized color-theme rainbow-mode dired-sort-menu magit window-number exec-path-from-shell use-package)))
 '(safe-local-variable-values
   (quote
    ((haskell-process-use-ghci . t)
     (haskell-indent-spaces . 4)
     (time-stamp-active . t)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
